import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { EntryService } from '../entry-service.service';
import { EntryLocationComponent } from '../entry-location/entry-location.component';
import { AuthService } from "../authentication.service";
import { Router } from "@angular/router";
import Swal from 'sweetalert2';

@Component({
  selector: 'app-list-entry',
  templateUrl: './list-entry.component.html',
  styleUrls: ['./list-entry.component.scss']
})
export class ListEntryComponent implements OnInit {

  constructor(private entryService: EntryService, public authService: AuthService, public router: Router,
    ) { }

  ngOnInit(): void {
    if(this.authService.isLoggedIn)
    {
      this.getEntryList();
    }
    else
    {
      this.router.navigate(['login']);
    }
  }

  ngAfterViewInit(){
    this.enableProgressBar = false;
  }

  entries;
  enableProgressBar = true;

  alternate: boolean = true;
  toggle: boolean = true;
  color: boolean = true;
  size: number = 30;
  expandEnabled: boolean = true;
  contentAnimation: boolean = true;
  dotAnimation: boolean = true;
  side = 'left';

  getEntryList = () =>
    this.entryService
      .getEntries()
      .subscribe(res => (this.entries = res));
  
  deleteEntry(data){
    Swal.fire({
      title: 'Delete Entry?',
      text: "Are you sure?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.entryService.deleteEntry(data).then(
          res => {
            Swal.fire(
              'Deleted!',
              'Entry has been deleted.',
              'success'
            );
          },
          err => {
            Swal.fire({
              icon: 'error',
              title: 'Error!!',
              text: 'Could not delete Entry!!!'
            });
          }
        );
      }
    });
  }
}