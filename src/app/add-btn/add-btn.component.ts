import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import * as moment from 'moment';
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import {FormBuilder, FormGroup} from "@angular/forms";
import Swal from 'sweetalert2';
import { EntryService } from '../entry-service.service';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {MatChipInputEvent} from '@angular/material/chips';
import { HereService } from "../here.service";

declare var H: any;

@Component({
  selector: 'app-add-btn',
  template: `
    <div>
      <button mat-fab color="accent" class="fab-bottom-right" (click)="newRecord()" matTooltip="New Entry">
        <mat-icon>add</mat-icon>
      </button>
    </div>
  `,
  styleUrls: [
    './add-btn.component.scss'
  ],
  providers:[EntryService]
})
export class AddBtnComponent implements OnInit {

  constructor(public dialog: MatDialog) { }

  ngOnInit(): void {
  }

  newRecord(){
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    // dialogConfig.height = '500px';
    dialogConfig.width = '350px';
    const dialogRef = this.dialog.open(DialogContentExampleDialog, dialogConfig);

    dialogRef.afterClosed().subscribe(result => {
    });

    dialogRef.afterOpened().subscribe(result => {
    });
  }

}
export class IconOverviewExample {}

@Component({
  selector: 'new-record-dialog',
  templateUrl: 'new-record-dialog.html',
})
export class DialogContentExampleDialog implements OnInit{

  form: FormGroup;

  latitude:number;
  longitude:number;

  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];

  people = [];
  locations = [];

  private platform: any;
  
  public query: string;
  public position: string;
  public address: any;

  customLocation = false;

  addPerson(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      this.people.push(value.trim());
    }

    if (input) {
      input.value = '';
    }
  }

  removePerson(data: any): void {
    const index = this.people.indexOf(data);

    if (index >= 0) {
      this.people.splice(index, 1);
    }
  }

  addLocation(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      this.locations.push(value.trim());
    }

    if (input) {
      input.value = '';
    }
  }

  removeLocation(data: any): void {
    const index = this.locations.indexOf(data);

    if (index >= 0) {
      this.locations.splice(index, 1);
    }
  }

  constructor(
    private fb: FormBuilder,
        private dialogRef: MatDialogRef<DialogContentExampleDialog>,
        @Inject(MAT_DIALOG_DATA) data, 
        private entryservice: EntryService,
        private here: HereService) {
          this.getLocation();
          this.form = fb.group({
            time:[moment().format("HH:mm")],
            date:[new Date()],
            latitude:[''],
            longitude:[''],
            addInfo:[''],
            peopleContacted:[[]],
            locationsVisited:[[]],
            customLocationText:['']
          });
          this.platform = new H.service.Platform({
            'apikey':'SKnBwWXYHw2OyvEl-BkRYQzbMl87ykB_V_PIgx75INE'
          }); 
    }
  
    ngOnInit(){
      
    }
  save(){
    if(!this.form.value.date)
    {
      Swal.fire({
        icon: 'error',
        title: 'Error!!',
        text: 'Enter Date'
      });
    }
    else if(!this.form.value.time)
    {
      Swal.fire({
        icon: 'error',
        title: 'Error!!',
        text: 'Enter Time'
      });
    }
    else if((!this.form.value.latitude || !this.form.value.longitude) && !this.customLocation)
    {
      Swal.fire({
        icon: 'error',
        title: 'Error!!',
        text: 'Location not found!! Enable location access in Site Settings'
      });
    }
    else if(this.customLocation && !this.form.value.customLocationText)
    {
      Swal.fire({
        icon: 'error',
        title: 'Error!!',
        text: 'Enter Location'
      });
    }
    else
    {
      let data = this.form.value;
      data.uid = JSON.parse(localStorage.user).uid;
      data.peopleContacted = this.people;
      data.locationsVisited = this.locations;
      if(this.customLocation)
      {
        delete data.latitude;
        delete data.longitude;
      }
      else
      {
        delete data.customLocationText;
      }
      this.entryservice.createEntry(data).then(
        res => {
          this.dialogRef.close(this.form.value);
          Swal.fire({
            icon: 'success',
            title: 'Success!!',
            text: 'Entry Added'
          });
        },
        err => {
          Swal.fire({
            icon: 'error',
            title: 'Error!!',
            text: 'Could not add Entry!!!'
          });
        }
      );
    }
  }

  getLocation(){
    if (navigator)
    {
      navigator.geolocation.getCurrentPosition( pos => {
          this.longitude = pos.coords.longitude;
          this.latitude = pos.coords.latitude;
          this.position = this.latitude.toString()+","+this.longitude.toString();
          this.getAddressFromLatLng();
        });
    }
  }

  public getAddressFromLatLng() { 
    if(this.position != "") {
        this.here.getAddressFromLatLng(this.position).then(result => {
            this.address = result;
        }, error => {
            console.error(error);
        });
    }
  }

  enableCustomLocation(event){
    if(event.checked)
      this.customLocation = true;
    else
      this.customLocation = false;
  }
}

